import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

public class AdministradorArchivo{

    public static void leerFicheros() throws IOException {
        FileInputStream file = new FileInputStream("/home/saul/Escritorio/productos.xlsx");

        XSSFWorkbook libro = new XSSFWorkbook(file);

        XSSFSheet hoja = libro.getSheetAt(0);

        Iterator<Row> filas = hoja.iterator();
        Iterator<Cell> celdas;

        Row fila;
        Cell celda;
        while(filas.hasNext()){
            fila = filas.next();
            celdas = fila.cellIterator();

            while(celdas.hasNext()){

                celda = celdas.next();

                switch(celda.getCellType()){

                    case NUMERIC:
                        System.out.println(celda.getNumericCellValue());
                        break;

                    case STRING:
                        System.out.println(celda.getStringCellValue());
                }
            }


        }
        libro.close();
    }

}
